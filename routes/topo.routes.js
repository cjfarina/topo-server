module.exports = app => {
  const login = require("../controllers/login.controller.js")
  const image = require("../controllers/image.controller.js")
  const auth = require('../config/auth')

  var passport = require('passport')
  var router = require("express").Router();

  router.route('/auth/facebook').post(passport.authenticate('facebook-token', { session: false }), login.facebook);
  router.route('/auth/me').get(auth.authenticate, login.getMe);
  router.route('/image/:user').get(auth.authenticate, image.userImage);
  router.route('/image/').get(image.get);
  router.route('/image/toggle').put(auth.authenticate, image.toggle);
  
  app.use('/api/v1', router);


};