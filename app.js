//mongoose file must be loaded before all other files in order to provide
// models to other modules

require('dotenv').config();
const db = require("./models");
const scheduler = require("./config/scheduler");
const express = require('express')
const router = express.Router()
const cors = require('cors')
const bodyParser = require('body-parser')


var passportConfig = require('./config/passport');

//setup configuration for facebook login
passportConfig();

var app = express();

function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if (!req.secure && req.get('x-forwarded-proto') !== 'https' && process.env.NODE_ENV !== "development") {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}
app.use(requireHTTPS);

// enable cors
var corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};

app.use(cors(corsOption));

//rest API requirements
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

router.route('/health-check').get(function (req, res) {
  res.status(200);
  res.send('Hello Pixel');
  console.log('Hello Pixel');
});

require("./routes/topo.routes")(app);

// Handle production
if(process.env.NODE_ENV === 'production'){
  // Static folder
  app.use(express.static(__dirname + '/public/'))

  //  Handle SPA
  app.get(/.*/m, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

const normalizePort = val => {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
};

port = normalizePort(process.env.PORT || '5000')
app.listen(port);
module.exports = app;

console.log('Server running at http://localhost:' + port);

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false 
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });


  scheduler.start()