const utils = require('./utils.js')

module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            buffer: [Number],
            length: Number,
            width: Number,
            height: Number
        },
        { timestamps: true }
    );

    schema.method("toJSON", utils.toJSON);
    schema.statics.ELEMENT_SIZE = 32
    schema.statics.BUFFER_INCREMENT = 1000

    schema.statics.next = async function () {
        let image = await this.findOneAndUpdate({}, { $inc: { length: 1 } });
        let pixel = image.length
        if (image.length == 0 || ((image.buffer.length - this.BUFFER_INCREMENT/2) * Image.ELEMENT_SIZE) == image.length) {
            var max32 = Math.pow(2, this.ELEMENT_SIZE) - 1
            array = Array.from({length: this.BUFFER_INCREMENT}, () => Math.floor((Math.random()-0.5) * max32));
            await this.findOneAndUpdate({}, { $push: { buffer: { $each: array } } });
        }
        return pixel
    };
    schema.statics.getCursor = function (pixel) {
        length = this.ELEMENT_SIZE
        let index = Math.floor(pixel / length)
        let offset = pixel % length
        return { index: index, offset: offset }
    };

    schema.statics.togglePixel = async function (pixel) {
        let cursor = this.getCursor(pixel)
        let operator = 1 << cursor.offset

        let selector = "buffer." + cursor.index
        return await this.findOneAndUpdate({ $bit: { [selector]: { xor: operator } } })
    };

    schema.statics.isOn = async function (pixel) {
        let cursor = this.getCursor(pixel)
        let selector = "buffer." + cursor.index
        return await this.countDocuments({ [selector]: { $bitsAllSet: [cursor.offset] } })
    };
    const Image = mongoose.model("Image", schema);
    return Image;
};

