const utils = require('./utils.js')
module.exports = mongoose => {
    var schema = mongoose.Schema({
        displayName: { type: String },
        name: {
            givenName: String,
            familyName: String
        },
        picture: String,
        firstTime: Boolean,
        email: {
            type: String,
        },
        facebookProvider: {
            type: {
                id: String,
                token: String
            },
            select: false
        }
    }, { timestamps: true }
    );

    schema.set('toJSON', { getters: true, virtuals: true });
    schema.method("toJSON", function () {
        let object = utils.toJSON.bind(this)();
        delete object['facebookProvider'];
        return object;
    });

    const User = mongoose.model('User', schema);
    return User;
};