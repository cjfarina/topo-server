const utils = require("./utils.js");

module.exports = (mongoose) => {
  var schema = mongoose.Schema({
    x: Number,
    y: Number,
    n: Number,
    isOn: Boolean,
    toggled: Boolean,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  });

  schema.method("toJSON", utils.toJSON);
  const Pixel = mongoose.model("Pixel", schema);
  return Pixel;
};
