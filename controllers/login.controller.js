const db = require("../models");
const jwt = require("jsonwebtoken");
const User = db.user;
const Image = db.image;
const Pixel = db.pixel;

// Create and Save a new Tutorial
exports.facebook = async (req, res) => {
  if (!req.user) {
    return res.send(401, "User Not Authenticated");
  }

  let user = req.user;
  req.auth = {
    id: user.id,
    email: user.email,
  };

  let firstTime = user.firstTime;

  let dbuser = await User.findOneAndUpdate(
    { _id: user.id },
    { firstTime: false }
  );
  let pixel = await Pixel.findOne({ user: dbuser._id });
  if (pixel == null) {
    pixel = await Pixel.aggregate([
      { $match: { user: null } },
      { $sample: { size: 1 } },
    ]);
    console.log("updating pixel: ", pixel[0]);
    pixel = await Pixel.findByIdAndUpdate(pixel[0]._id, { user: dbuser._id });
  }
  req.token = jwt.sign({ user: req.auth }, process.env.JWT_SECRET, {
    expiresIn: 60 * 60 * 24 * 7,
  });
  // console.log(user)
  res
    .status(200)
    .send({ token: req.token, firstTime: firstTime, user: user, pixel: pixel });
};

exports.getMe = async (req, res) => {
  let user = await User.findById(req.auth.id);
  req.user = user;
  res.send({ user: user });
};

exports.upsertFbUser = async (accessToken, refreshToken, profile, cb) => {
  let user = null;
  try {
    user = await User.findOne({ "facebookProvider.id": profile.id });
    console.log(profile);
    if (!user) {
      user = await User.create({
        displayName: profile.displayName,
        name: {
          givenName: profile.name.givenName,
          familyName: profile.name.familyName,
        },
        firstTime: true,
        picture: profile.photos[0].value,
        email: profile.emails[0].value,
        facebookProvider: {
          id: profile.id,
          token: accessToken,
        },
      });
    }
    cb(null, user);
  } catch (err) {
    cb(err, user);
  }
};
