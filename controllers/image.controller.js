const db = require("../models");
const _ = require("lodash");
const Image = db.image;
const User = db.user;
const Pixel = db.pixel;
const Pusher = require("pusher");
const auth = require("../config/auth");
const { authenticate } = require("passport");

const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_APP_KEY,
  secret: process.env.PUSHER_APP_SECRET,
  cluster: process.env.PUSHER_APP_CLUSTER,
});

exports.get = async (req, res) => {
  let image = await Image.findOne();
  // await new Promise((resolve) => setTimeout(resolve, 5000));
  res
    .status(200)
    .send({ image: image, pixel: { id: -1, isOn: _.sample([true, false]), x: Math.floor(Math.random()*1000), y: Math.floor(Math.random()*1000) } });
};

exports.userImage = async (req, res) => {
  let image = await Image.findOne();
  let user = await User.findById(req.params.user);
  if (user == null) {
    res.status(400).send("Error getting user.");
    return;
  }
  let pixel = await Pixel.findOne({ user: user._id });
  res.status(200).send({ image: image, pixel: pixel, user: user });
};

exports.toggle = async (req, res) => {
  try {
    console.log("toggling pixel");
    let pixel = req.body.pixel;
    console.log("toggling pixel", pixel);
    await Image.togglePixel(pixel);
    let isOn = await Image.isOn(pixel);
    pixel = await Pixel.findOneAndUpdate({ n: pixel }, { isOn: isOn });
    console.log("x-socket", req.headers["x-socket-id"]);
    pusher.trigger(
      "topo",
      "toggle",
      {
        pixels: [{ n: pixel.n, x: pixel.x, y: pixel.y, isOn: isOn }],
        pusherSessionID: req.body.pusherSessionID,
      },
      req.headers["x-socket-id"]
    );
    res.status(200);
    res.send({ message: "Pixel " + pixel + " toggled." });
    console.log("end toggling pixel");
  } catch (error) {
    console.log(error);
    res.send(500, error.message);
  }
};
