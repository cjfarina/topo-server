require("dotenv").config();
const db = require("../models");
const User = db.user;
const Image = db.image;
const Pixel = db.pixel;

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

async function seed() {
  await User.deleteMany();
  await Image.deleteMany();
  await Pixel.deleteMany();

  const ELEMENT_SIZE = Image.ELEMENT_SIZE;
  let max32 = Math.pow(2, ELEMENT_SIZE) - 1;
  let width = 1000
  let height = 1000
  let array_length = (width * height) / ELEMENT_SIZE;
  array = Array.from({ length: array_length }, () =>
    Math.floor((Math.random() - 0.5) * max32)
  );
  let image = await Image.create(
    { length: array_length * ELEMENT_SIZE, width: width, height: height, buffer: array }
  );
  // await print(array)
  // console.log('Printing image')
  // await print((await Image.findOne()).buffer);
  // console.log('Printing is on')
  // await printIsOn(await Image.findOne());
  for (let n = 0; n < array_length * ELEMENT_SIZE; n++) {
    let isOn = await Image.isOn(n);
    let y = Math.floor(n / width)
    let x = n % width
    let data = { n: n, isOn: isOn, x: x, y: y, toggled: false }
    pixel = await Pixel.create(data);
    if(x == 0){
      console.log('new row', data)
    }
    // console.log('new row', data)
  }
  process.exit();
}

async function print(buffer) {
  for (element of buffer) {
    console.log((element >>> 0).toString(2).padStart(32, 0).split("").reverse().join(""));
  }
}
async function printIsOn(image) {
  for (let n = 0; n < image.length; n++) {
    let isOn = await Image.isOn(n);
    if ((n+1) % Image.ELEMENT_SIZE == 0) {
      console.log(isOn ? "t" : "f");
    } else {
      process.stdout.write(isOn ? "t" : "f");
    }
  }
  console.log()
}

seed();
