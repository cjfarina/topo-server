const db = require("../models");
const jwt = require('jsonwebtoken')
const crypto = require('crypto');
const User = db.user;
const Image = db.image;


db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });


async function main() {
    
    let buffer =  [5, 10, 115]
    // let image = await Image.findOneAndUpdate({buffer: buffer})
    let image = await Image.findOne()

    console.log('buffer')
    print(image.buffer);

    console.log(await Image.isOn(0))
    // image = await Image.togglePixel(30)

    // console.log('after update')
    // print(image.buffer);

    process.exit();
};

async function next(){
    await Image.findOneAndUpdate({}, {length:0, buffer: []}) 

    for(var i = 0; i < 10; i++){
        console.log(await Image.next())
    }

    process.exit();
}

async function print(buffer){
    for(byte of buffer){
        console.log((byte >>> 0).toString(2).padStart(32, 0).split("").reverse().join(""))
    } 
}

// toggle();

// next();
main();
