const db = require("../models");
const User = db.user;
const Image = db.image;


db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });
    
const { workerData, parentPort } = require('worker_threads')

// You can do any heavy stuff here, in a synchronous way
// without blocking the "main thread"


for (var i = 0; i < 100; i++) {
    console.log(Image.next())
}
parentPort.postMessage({ hello: workerData })