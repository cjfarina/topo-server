const db = require("../models");
const User = db.user;
const Image = db.image;


db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });


// index.js
// run with node --experimental-worker index.js on Node.js 10.x
const { Worker } = require('worker_threads')

function runService(workerData) {
    return new Promise((resolve, reject) => {
        const worker = new Worker('./scripts/service.js', { workerData });
        worker.on('message', resolve);
        worker.on('error', reject);
        worker.on('exit', (code) => {
            if (code !== 0)
                reject(new Error(`Worker stopped with exit code ${code}`));
        })
    })
}

async function run() {
    Image.findOneAndUpdate({}, { length: 0, buffer: [] })
    for (let index = 0; index < 100; index++) {
        const result = await runService('world')
        console.log(result);
    }

}

run().catch(err => console.error(err))