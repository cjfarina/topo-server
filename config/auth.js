const expressJwt = require('express-jwt')
module.exports.authenticate = expressJwt({
    secret: process.env.JWT_SECRET,
    requestProperty: 'auth',
    getToken: function (req) {
      if (req.headers.authorization) {
        const token = req.headers.authorization.replace("Bearer ", "");
        if (token) {
          return token
        }
      }
      return null
    }
  });