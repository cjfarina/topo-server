const db = require("../models");
const Pixel = db.pixel;
const Pusher = require("pusher");
const Image = db.image;

const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_APP_KEY,
  secret: process.env.PUSHER_APP_SECRET,
  cluster: process.env.PUSHER_APP_CLUSTER,
});

const MAX_PIXELS = 2;

async function updateImage() {
  
  let extraPixels = [];
  let updated = [];
  if (MAX_PIXELS > 0) {
    let sampleSize = MAX_PIXELS;
    extraPixels = await Pixel.aggregate([
      { $match: { user: null } },
      { $sample: { size: sampleSize } },
    ]);
    for (const pixel of extraPixels) {
      await Image.togglePixel(pixel.n);
      p = await Pixel.findByIdAndUpdate(
        pixel._id,
        { isOn: !pixel.isOn}
      );
      updated.push(await Pixel.findById(p._id))
    }
    pusher.trigger("topo", "toggle", { pixels: updated });
  }
}

// async function updateImage() {
//   console.log('update image')
//   pixels = await Pixel.find({ toggled: true });
//   if(pixels.length > 0){
//     console.log('updating image', pixels)
//   }
//   await Pixel.updateMany({toggled: true}, {$set: {toggled: false}})
//   console.log('end update image')
//   let extraPixels = [];
//   let updated = [];
//   if (pixels.length < MAX_PIXELS) {
//     let sampleSize = MAX_PIXELS - pixels.length;
//     extraPixels = await Pixel.aggregate([
//       { $match: { user: null } },
//       { $sample: { size: sampleSize } },
//     ]);
//     for (const pixel of extraPixels) {
//       await Image.togglePixel(pixel.n);
//       p = await Pixel.findByIdAndUpdate(
//         pixel._id,
//         { isOn: !pixel.isOn}
//       );
//       updated.push(await Pixel.findById(p._id))
//     }
//   }

//   all = pixels.concat(updated).map((pixel) => {
//     return { n: pixel.n, x: pixel.x, y: pixel.y, isOn: pixel.isOn };
//   });

//   if(all.length > 0){
//     console.log('trigger', all)
//     pusher.trigger("topo", "toggle", { pixels: all });
//   }
// }




module.exports.start = () => {
  console.log(process.env.ENABLE_SCHEDULER)
  if(process.env.ENABLE_SCHEDULER=='true'){
    setInterval(updateImage, 15000);
  }
  // setInterval(updateImage, 2000);
};
