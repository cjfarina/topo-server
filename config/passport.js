'use strict';

var passport = require('passport'),
  FacebookTokenStrategy = require('passport-facebook-token'),
  User = require('mongoose').model('User');

const login = require("../controllers/login.controller.js")

module.exports = function () {

  passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    fbGraphVersion: 'v3.0'
  },
    function (accessToken, refreshToken, profile, done) {
      login.upsertFbUser(accessToken, refreshToken, profile, function (err, user) {
        return done(err, user);
      });
    }));
};

